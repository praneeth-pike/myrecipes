require 'test_helper'

class ChefTest < ActiveSupport::TestCase
  
  def setup
    @chef = Chef.new(chefname: "John", email: "john@example.com")
  end
  
  test "Chef should be valid" do
    assert @chef.valid?
  end
  
  test "Chef name should be present" do
    @chef.chefname = " "
    assert_not @chef.valid?
  end
  
  test "Chef name should no tbe too long" do
    @chef.chefname = "a" *41
    assert_not @chef.valid?
  end
  
  test "Ched name should not be too short" do
    @chef.chefname = "aa"
    assert_not @chef.valid?
  end
  
  test "email should be present" do
    @chef.email = " "
    assert_not @chef.valid?
  end
  
  test "email length should be within bounds" do
    @chef.email = "a" * 101 + "@example.com"
    assert_not @chef.valid?
    
  end
  
  test "email should be unique" do
    dup_chef = @chef.dup
    dup_chef.email = @chef.email.upcase
    @chef.save
    assert_not dup_chef.valid?
  end
  
  test "Email validation should accept valid addresses" do
    valid_addresses = %w[user@eee.com R_TDD-DS@eee.hello.org user@example.com first.last@eem.au laura+joe@monk.in]
    valid_addresses.each do |va|
      @chef.email = va
      assert @chef.valid?, '#{va.inspect} should be valid'
    end
  end
  
  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_At_eee.org user.name@example. eee@i_am_.com]
    invalid_addresses.each do |iv|
      @chef.email = iv
      assert @chef.valid?
    end
  end
  
end